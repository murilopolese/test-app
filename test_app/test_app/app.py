import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QVBoxLayout,
                             QPushButton, QComboBox, QTextEdit)

class TestApp(QWidget):
    combo = None
    button = None
    textarea = None

    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        layout = QVBoxLayout()
        self.setLayout(layout)

        self.combo = QComboBox(self)
        self.combo.addItem('Item 1')
        self.combo.addItem('Item 2')
        self.combo.addItem('Item 3')

        self.button = QPushButton('Button', self)

        self.textarea = QTextEdit(self)
        self.textarea.setReadOnly(True)

        layout.addWidget(self.combo)
        layout.addWidget(self.button)
        layout.addWidget(self.textarea)

        self.button.clicked.connect(self.buttonClicked)
        self.combo.currentIndexChanged.connect(self.comboChanged)
        
        self.setGeometry(100, 100, 300, 300)
        self.setWindowTitle('Test App')
        self.show()

    def comboChanged(self, i):
        self.textarea.append('combo changed')
        self.textarea.append(self.combo.currentText())
        print('combo changed', self.combo.currentText())

    def buttonClicked(self):
        self.textarea.append('button clicked')
        print('button clicked')


def main():
    app = QApplication(sys.argv)
    flasher = TestApp()
    sys.exit(app.exec_())
